---
slug: /ethics
title: Ethics
toc_max_heading_level: 6
---

## In short

### Things I believe in

* 💡&nbsp;**Friendship**, as one of the greatest experiences in life.
* 💡&nbsp;**Romantic love**, although not in the absolute, binary, eternal form so often portrayed in fiction or by religion.
* Other forms of 💡&nbsp;**love**.
* 💡&nbsp;**Gay marriage** (and, in general, any &ldquo;right&rdquo; that puts disadvantaged minorities *on par* with other members of
  society).
* Radical 💡&nbsp;**euthanasia** (apart from not processing doctors nor relatives who assist in a suicide, the State should actively help those
  who *really* want to end their lives, but who cannot do that by themselves <span class="icon ref">👉</span>&nbsp;[**1**](footnotes#1)).
* 💡&nbsp;**Surrogate pregnancies**:
  [this article (in Spanish)](https://blogs.elconfidencial.com/economia/laissez-faire/2017-02-10/gestacion-subrogada_1329076/)
  summarises well my position.
* 💡&nbsp;**Altruistic donations by billionaires**
  ([SSC: &ldquo;Against against billionaire philanthropy&rdquo;](https://slatestarcodex.com/2019/07/29/against-against-billionaire-philanthropy/)).

### Things I'm not sure about

#### Gays adopting children

I *do* support 💡&nbsp;**gay adoption**, by principle.
I do not believe that gay couples (male or female) necessarily lack any of the attributes that are important for parenthood.
Experience and science seem to have proved that children of gay couples are, on average, as healthy and happy as other children.

But I suspect there might be moral reasons to give always prefence to straight couples waiting to adopt children, *ceteris paribus*.

#### Free abortion

I feel that 💡&nbsp;**abortion** is *not* a yes-or-no issue.
One sign of that is that virtually everybody opposes abortion very late in the pregnancy (even strong supporters of abortive rights do not condone aborting big,
perfectly viable, completely formed fetuses that could be safely delivered in the last days/weeks of pregnancy).
Between that, and the morning-after pill, I see a spectrum of possibilities.
It's worrying that so often the &ldquo;abortion debate&rdquo; is framed in black-or-white terms.

My doubts about free abortion do not have anything to do with religion (I'm an atheist).

I do not think that the way the feminist cause has appropriated the debate is fair, nor healthy for anyone.
Of course I understand that this is not a symmetric issue, for purely biological reasons.
But I see some circumstances (perhaps *many* circumstances) where the opinion of a responsible father is of value, and should be heard.
At the very least, slogans like &ldquo;our body, we decide&rdquo;, &ldquo;it's *my* choice&rdquo;, etc. are simplistic, exclusive and polarising.

### Things I oppose

* 💡&nbsp;**Death penalty** (it's both immoral and ineffective).
* 💡&nbsp;**Collective guilt**, 💡&nbsp;**historic guilt** (nobody can be held accountable for the crimes of their close
  relatives; much less of their ancestors; let alone imagined or supposed distant ancestors with whom one barely shares a religion, a language, or an
  approximate skin colour).

## &ldquo;Immoral by proxy&rdquo;

\[Pending.\]

## Honour vs. dignity

💡&nbsp;**Honour** is an outdated concept.
Society has evolved
[from &ldquo;cultures of honour&rdquo; to &ldquo;cultures of dignity&rdquo;](http://heterodoxacademy.org/2016/03/26/victimhood-culture-at-emory/).
Honour is acquired through conflict and violence, and relies on the opinion of others.
💡&nbsp;**Dignity**, on the other hand, is universal and egalitarian.
