---
slug: /life
title: Life
toc_max_heading_level: 6
---

What is most important to me.

## Priorities

The most important thing for me should be my own **health**, **happiness** and general **well-being**.

[Perhaps [being &ldquo;interesting&rdquo;](https://www.youtube.com/watch?v=U88jj6PSD7w) is a better goal than being happy.]

**People** around me, people who are important to me, should be my second concern.

> _&ldquo;Father, give us courage to change what must be altered,  
  serenity to accept what cannot be helped,  
  and the insight to know the one from the other.*&rdquo;_

&mdash;&nbsp;[Serenity prayer](https://yalealumnimagazine.com/articles/2709-you-can-quote-them)

## General principles, and making decisions

These balances are difficult in life:

* Weighing my own **selfish interests** against abstract **moral principles**.
  Doing ***the right thing*** also when no-one's looking.
  Complying with rules even when the global consequences of doing otherwise are negligible or debatable.
  Example: dodging taxes.
* Perhaps put in a different way: pursuing **my own self-interest** vs. being **altruistic** (ie, sacrificing things for the sake of others).
* Balancing the importance of having **money** vs. the importance of having **time**.
* Learning to what extent **family** is important (keeping good bonds with relatives, being involved with extended family and in-laws, working on a long-lasting
  romantic partnership, having children, spending most of your time and resources on your children, etc).
* Learning the righ balance between **pleasure** and **education** or investments in the future.
* What's the right level of **risk** in life (for all kinds of decisions and changes).
* The balance between abstract intellectual principles on the one hand (what one should say and do, *in theory*), and social norms and pragmatism on the other.
* The balance between justly boasting about my merits and staying always humble.
* I don't want to be vein and boring when I'm an old man.
  As men age, they grow more boastful.
  You can see that trait clearly in some old people; with their tendency to blow their own little trumpets at every opportunity.
  I shall be vigilant and stay humble.
* Good old stoicism is an asset.

The **merit of deeds** should be judged considering solely the information available **to that person** and **at the time decisions were made, or actions were taken**&nbsp;&mdash;&nbsp;never considering their consequences, to the extent that they were impossible to predict.
Positive example: someone all of a sudden spends all their life savings on lottery tickets, and they win the biggest prize in history. Their call to spend all their money in lottery was still dumb and absolutely wrong (as it always is). Regardless of that fantastic but hugely unlikely outcome.
Negative example: [TO-DO].

_“Decisions based on emotions aren't decisions at all.”_ (House of Cards, S01E12). One should not make non-trivial **decisions** in the heat of the moment.

The idea of having, or achieving, “one's dream” (singular), or “the dream of one's life” seems weak, and maybe even noxious. By definition, pursuing such “dream” should be the ultimate goal in life, and its achievement has to bring immediate happiness at an unprecedented level. It seems imposible to know that beforehand. Also, circumstances, desires and priorities in life change wildly. But such an important, long-term “dream” should not be subject to those petty changes. Also, by achieving that goal (if you ever do that), you automatically downgrade your life, your ambitions and expectations, ie *nothing* can ever be as huge, as important, or as life-changing as achieving your dream in life, right?

Always remember that you tend to idealise the **past**.

> *&ldquo;You can't go very far wrong if you're **motivated by love** and **guided by reason**.&rdquo;*

&mdash;&nbsp;[Sam Harris](https://youtu.be/aALsFhZKg-Q?t=5158).

> *&ldquo;I just sit in my office and read all day.&rdquo;*

&mdash;&nbsp;[Warren Buffett](https://www.gatesnotes.com/About-Bill-Gates/Grilling-and-chilling-with-Warren-Buffett).

> *&ldquo;Relentlessly prune bullshit, don't wait to do things that matter, and savor the time you have. That's what you do when life is short.&rdquo;*

&mdash;&nbsp;[Paul Graham](http://paulgraham.com/vb.html).

> *&ldquo;Don't ignore your dreams; don't work too much; say what you think; cultivate friendships; be happy.&rdquo;*

&mdash;&nbsp;[Paul Graham](http://paulgraham.com/todo.html).

[<span class="icon">👤</span>&nbsp;**Jordan Peterson**'s answer to the question &ldquo;what are the most valuable things everyone should know&rdquo; on
Quora](https://www.quora.com/What-are-the-most-valuable-things-everyone-should-know/answer/Jordan-B-Peterson)

💡&nbsp;**Pride** and 💡&nbsp;**shame** apply only to aspects of life over which one has considerable control.

💭&nbsp;Example: it does *not* make sense to &ldquo;feel proud (nor ashamed) to be an American / a woman / gay / white / tall&hellip;&rdquo;
At most, one is only partially responsible of being &ldquo;a Dutch&rdquo; (citizenships can be lost and gained, to some extent, through our actions), or
&ldquo;physically fit&rdquo; (you cannot control genetics, but your lifestyle influences your phenotype).

💡&nbsp;**Nationality**, in particular, is, in the vast majority of cases, an accident, completely involuntary.
(Exceptions are those who migrate purposefully, break off with their country of origin, etc.)
Modulo those few people, no-one can truly feel proud, nor ashamed, of their nationality or their mother tongue.

> *&ldquo;Those who have accomplished nothing as individuals feel compelled to be proud of their race.&rdquo;*

&mdash;&nbsp;<span class="icon name">👤</span>&nbsp;[**Jordan Peterson**](https://twitter.com/jordanbpeterson/status/935380897351680000)

## People

* Explicitly asserting 💡&nbsp;**non–romantic love** (to relatives and close friends) [seems to be necessary and have a huge positive impact on long-term happiness](http://paulgraham.com/todo.html). It isn't always natural nor easy, though.
* 💡&nbsp;**Love** isn't an absolute, and it doesn't manifest itself in a perfect form. There is no “perfect match”, nor a “soul mate” ultimately waiting for
you.
* In most day–to–day situations, 💡&nbsp;**lying** is **bad** and shall be avoided.
There are a number of circumstances, though, when lying is perfectly admisible&nbsp;&mdash;&nbsp;and even advisable.
Always telling the 💡&nbsp;**truth** is overrated.
Few valid exceptions to this rule may be lying (or simply hiding part of the truth) to: children, mentally incapable adults, terminally ill patients.
There are situations in which a *lack* of information is good for you.
But those are the exception.
* One shall take no 💡&nbsp;**offence** from sources one does not value or respect.
  If someone I despise calls me a name, or tries to offend me, I can safely ignore them (as long as their public defamation or verbal violence doesn't actually
  affect my image or reputation).
  :point_right:[&#10033;&nbsp;**honour vs. dignity**](ethics##3-honour-vs-dignity)
* One can't **please everybody**. There isn't one way of being, behaving or talking that everybody always likes and approves. Even the most likeable, charming, thoughtful person can be annoying to some people, sometimes. Therefore, realising that somebody just doesn't like you, for no particular reasons, might well be irrelevant news&nbsp;&mdash;&nbsp;it doesn't always need to trigger self-evaluation nor reflection, let alone guilt.
* **When I see in other people habits or traits of personality that I despise**, sometimes it is because deep down I know I have that very same defect.
I'll do well if I stop and reflect honestly when I detect those unpleasant behaviours in others; when I can actually recognise myself on those
&ldquo;defects&rdquo;, it is a humbling and precious pragmatical lesson (to either be more tolerant with others and/or work on correcting that bad habit).

## Debates and discussing with people

* All 💡&nbsp;**debates** should be an open–minded exchange of ideas, and a collaborative quest to put them to test in order to identify the best ones.
That's the goal.
Everything else is a colourful wrapping.
* Too often, the environment isn't appropriate for a debate: loud noise, big egos, bad manners, haste and the lack of a minimum conceptual base to start with
are common.
Under such conditions, trying to debate is fruitless at best, and often frustrating.
Avoid.
* Always apply the [principle of charity](https://en.wikipedia.org/wiki/Principle_of_charity).
* It makes no sense to talk about the &ldquo;winner&rdquo; of a debate.
* It is absurd when people say things like *&ldquo;I know what is true; and no matter what you say, I won't change my mind!&rdquo;* or *&ldquo;I don't care what
  you say or what your arguments are; you're never going to convince me!&rdquo;*.
  That approach to discussion *alone* disqualifies anyone in a rational discussion; it is much worse than using an *ad hominem* argument, or wielding faulty
  logic.
  Never say that (and never *think* that).

* [Paul Graham: *&ldquo;How to Disagree&rdquo;*](http://www.paulgraham.com/disagree.html)
* [Bryan Caplan: *&ldquo;Silence is Stupid, Argument is Foolish&rdquo;*](https://www.econlib.org/silence-is-stupid-argument-is-foolish/):
  _&ldquo;remain calm, take nothing personally, use probabilities, face hypotheticals head-on, and spurn Social Desirability Bias like the plague&rdquo;_.

Many of my frustrations in trying to understand and debate others arise from <span class="icon ref">👉</span>&nbsp;[my personality type](/personality#i).

https://aus.social/@space_cadet/105938996234546309

## Knowledge

Learning and discovering is one of the greatest pleasures in life.

I love [what 👤&nbsp;**Steven Pinker** says here about the role of universities and what an &ldquo;educated&rdquo; person is](https://fs.blog/steven-pinker-broad-education/) ([video](https://web.archive.org/web/20201123052326/https://www.youtube.com/watch?v=5eo3ZC1Sc2w)).

## Goals and obsessions

I believe more [in <span class="icon idea">💡</span>&nbsp;**persistence** and in <span class="icon idea">💡</span>&nbsp;**practice**
than in <span class="icon idea">💡</span>&nbsp;**talent**](https://www.gatesnotes.com/Books/Mindset-The-New-Psychology-of-Success).

A *healthy* dose of <span class="icon idea">💡</span>&nbsp;**obsession** seems necessary to achieve great things.
At least, so far I don't seem to be able to accomplish almost nothing that is difficult and important (according to my own definition of &ldquo;important&rdquo;) without becoming at least a bit obsessed about it for some time.

(Some time after writing this, I'm pleased to learn that <span class="icon name">👤</span>&nbsp;**Paul Graham** no less
[seems to agree with me about the critical importance of obsession](http://paulgraham.com/genius.html).)

An exception might be: getting my first MSc (in CS and software engineering, between the ages of 18 and 24), which was both difficult and important.
But somehow I don't think that counts: I *wanted* it, I *mostly* liked it, I didn't even have a plan B at the time, it was partly inertia (being curious, introverted, not too dumb), and it was mostly unexceptional in the context of my relatives and friends.

### Goals accomplished

At times I have set specific goals for myself:

> *&ldquo;Run 10–km races…☑*  
  *Get 2nd master's degree in foreign country…☑*  
  *Run ½ marathons…☑*  
  *Get certified in basic Japanese…☑*  
  *Run a marathon…☑*  
  *¿?&rdquo;*

&mdash;&nbsp;[Me, in Apr 2013](https://twitter.com/tripu/status/328640582383706112)

What I have never done, so far, is defining plans, milestones or deadlines for those goals.
Perhaps that's where I should improve my method&hellip;

Now I would add, as new &ldquo;goals accomplished&rdquo;:

* Own and enjoy a decent motorbike (it was [a Honda CBF600](https://www.moterus.es/motero/tripu/motos/honda-cbf-600-sa))&hellip;&nbsp;☑
* Own and enjoy an even better motorbike (it is [a BMW F800GT](https://www.moterus.es/motero/tripu/motos/bmw-f-800-gt))&hellip;&nbsp;☑
* Run (and finish) *another* marathon (abroad: [NYC Marathon 2017](https://flow.polar.com/training/analysis/1926907684))&hellip;&nbsp;☑
* Live and work in Japan, in Tokyo if possible (I lived in Tokyo for one year and nine months)&hellip;&nbsp;☑
* Use **Duolingo** _every day_ for a long time; have a very long streak
  (I used it to study tiny bits of German and Japanese [for two years, uninterruptedly](https://qoto.org/@tripu/104265163260232968))
* Have a child; have _children_, if possible (I have one daughter)
* Take the [_&ldquo;Giving What We Can&rdquo;_ Pledge](https://www.givingwhatwecan.org/pledge/) (ie, always donate 10% of my gross earnings)

### Goals pending

&hellip;and, as &ldquo;new goals&rdquo;, or &ldquo;old goals still pending&rdquo;, things like:

* Live and work abroad again, this time with my family
* Travel for at least a few months around the world (not necessarily *around* it, but you get the idea)
* Retire &ldquo;early&rdquo;
* Own and enjoy _the_ motorbike that I like the most, without (financial) restrictions
* Write a novel, publish it if possible ([NaNoWriMo](http://www.nanowrimo.org/)?)
* Write a non-fiction book, publish it if possible
* Publish articles or columns somewhere (digital outlet, local newspaper, etc)
* Get to speak and write German well
* Get to speak and write Japanese well
* Be stronger (significantly, both in strengh and in aspect)
* Own a house (a *nice* house, if possible)
* Develop (or lead) an open-source project that becomes well known or reaches great impact
* Make my living off a software product/service/company of my own
* Be &ldquo;rich&rdquo;

### Goals failed

(Hint: I'm 40 at the time of updating this list.)

* Be a young parent (too old already)
* Have *many* (read: &ldquo;three or more&rdquo;) children (seems almost impossible for biological, financial and personal reasons)
* Be *really* fluent in a language other than Spanish (too late&hellip; unless I got to live and work in Italy or in Catalonia for the next twenty or thirty years of my life, perhaps)
* Be quite good at some sport (I don't mean &ldquo;professional&rdquo;, but even as an *amateur* it's too late)
* Be a good musician (ditto)
* Be a good illustrator, designer or painter (probably too late, although I'm not completely sure)
* Benefit from the compounding effects of early systematic investment (I bought my first bond at around 27, and my first stocks at 34)

## Measures, estimates and models

* _Everything_ can be either measured directly, or estimated
* _Everything_ can be modelled
* Measuring and modelling something is usually useful to make decisions (ie, all the time)
* At worst, your measures &amp; model don't give you additional information, or your confidence in their accuracy is so low that you discard them (no loss)
* When measuring &amp; modelling costs more than the value you expect to gain from the insights it provides, you don't do it (ie, very often in day-to-day life)

Support for this:

* [Myself in May 2022](https://qoto.org/@tripu/108376014503251403)
* [PUTANUMONIT](https://putanumonit.com/2015/11/03/002-credo/):
  - &ldquo;You can put a number on anything if you try hard enough (number quality not guaranteed, see store for details)&rdquo;
  - &ldquo;Once you put a number on something, you improve your understanding and decision making (even if the number isn't of prime quality)&rdquo;
  - &ldquo;The world we live in is made of math, however literally you decide to take that statement&ldquo;
  - &ldquo;Whenever a field of science achieves any useful knowledge of that world, it is usually in form of precise mathematical
    equations or careful statistics. Every science is an exact science, or trying to be.&rdquo;
* [Doug Hubbard on the Clearer Thinking Podcast](https://podcast.clearerthinking.org/episode/211/doug-hubbard-measuring-everything-that-matters/):
  - &ldquo;Take something so squishy like love. Could you really measure love?&rdquo;
    > &ldquo;Well, has anyone ever seen it? I think a lot of people would say, &lsquo;Yes.&rsquo; And then, I might follow up with, &lsquo;Have you seen two people that were more in love than some other two people?&rsquo; They would say, &lsquo;Well, yeah, of course.&rsquo; And then I would say, &lsquo;So, what did you see when you saw more love? Describe what you saw.&rsquo; And then they would end up describing some observable consequences. As soon as you're figuring out how you observe it, then really, all you need are some procedures for collection of data in the math world, that's it. It's also very useful to figure out why you need to know it. For example, if you're interested in figuring out whether or not a married couple will stay together because you're a community bank and you give out loans to family-owned businesses, maybe you're interested in how long they would stay together. Well, actually, there is some reason to believe that some near-term observations over a short period of time actually correlate with whether or not they would stay married for a longer period of time. If that's what you're actually interested in, then you've got some near-term observable things that are relevant to something you're trying to forecast. But a lot of reasons that things seem immeasurable, like love, is people haven't really quite figured out what they meant by it in the first place. As soon as they figure out what they see when they see more of it, they're well on the way to the path of measuring something. And the other thing is, if it has consequences at all, if it matters in the least, it has to be observable somehow. And not even just indirectly either it can be directly observable.&rdquo;
  - &ldquo;What is value of information? Why should people care about it?&rdquo;
    > &ldquo;You should not be measuring everything because there isn't an economic value to measurement.
      And when the cost of a measurement exceeds the value of it, you don't do it.
      That's the main reason not to measure something — it's just the economic constraints.&rdquo;

## Pending ideas

* [100 Tips for a Better Life](https://www.lesswrong.com/posts/7hFeMWC6Y5eaSixbD/100-tips-for-a-better-life)
* [100 Ways To Live Better](https://www.lesswrong.com/posts/HJeD6XbMGEfcrx3mD/100-ways-to-live-better)
* https://twitter.com/waitbutwhy/status/1406980353986809861
* [103 Bits of Advice I Wish I Had Known](https://kk.org/thetechnium/103-bits-of-advice-i-wish-i-had-known/)
