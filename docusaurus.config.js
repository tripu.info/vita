module.exports = {
    title: 'Vīta',
    tagline: '📜 Life notes',
    url: 'https://vita.tripu.info/',
    baseUrl: '/',
    favicon: 'img/favicon.png',
    titleDelimiter: '/',
    onBrokenLinks: 'throw',
    onBrokenMarkdownLinks: 'throw',
    onDuplicateRoutes: 'throw',
    presets: [
        [
            'classic',
            {
                docs: {
                    routeBasePath: '/'
                },
                blog: false
            }
        ]
    ],
    themeConfig: {
        image: 'img/avatar.png',
        colorMode: {
            disableSwitch: true
        },
        navbar: {
            title: 'Vīta',
            items: [
                {
                    label: 'Contribute',
                    to: 'https://gitlab.com/tripu.info/vita',
                    position: 'right'
                },
            ],
            logo: {
                alt: 'tripu',
                src: 'img/avatar.png'
            }
        }
    }
};
